<?php
/**
 * Created by PhpStorm.
 * User: rafiquee
 * Date: 2018-12-09
 * Time: 16:03
 */

namespace App\Service;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogService extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{

    public function getEntityManager()
    {
        $em = $this->getDoctrine()->getManager();
        return $em;
    }

}