<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentForm;
use App\Form\CommentFormType;
use App\Form\CommentType;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Faker\Factory;


class PostController extends Controller
{
    /*
     * add one record to db -> dodaj post
     * */
    /**
     * @Route("/post", name="post")
     */
    public function index()
    {
        $faker = Factory::create();

        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->setAuthor($faker->userName);
        $post->setTitle($faker->title);
        $post->setContent($faker->text);
        $post->setPreContent($faker->text);
        $post->setCreatedAt($faker->dateTime);
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($post);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response($post->getAuthor().' write new post. <a href="/"> back</a>');
    }

    /*
     * shows all posts on the site, use KnpPaginator bundle
     * */
    /**
     * @Route("/", name="view")
     */
    public function viewall(Request $request)
    {

        $this->arr[] = 'jablko';


        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository(Post::class)->findAll();

        /* @var $paginator \knplabs\knp-paginator-bundle */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $request->query->get('page',1), 10);

        return $this->render('base.html.twig', array('posts' => $pagination));
    }

    /*
    * shows one post on the site and all comments
    * */
    /**
     * @Route("/article/{id}", name="post_show")
     */
    public function viewComment(Post $post, Request $request)
    {
    $form = null;
        //if below suggest only register used could read posts
        //if ($user = $this->getUser()) {
            $user = $this->getUser();
            $comment = new Comment();
            $comment->setPost($post);
            $comment->setUser($user);

            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request); //$request zawiera wszystkie informacje pobrane z db za pomoca viewall()

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($comment);
                $entityManager->flush();

                $this->addFlash('success', 'comment added');

                return $this->redirectToRoute('post_show', array('id' => $post->getId()));
            }
        //}

        return $this->render('comment.html.twig', array(
            'post' => $post,
            'form' => is_null($form) ? $form : $form->createView() //jesli uzytkownik nie jest zalogowany zwraca nula i wywala appke
        ));
    }

}
