<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Address;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create();
        for ($i = 0; $i < 20; $i++) {
            $product = new Post();
            $product->setTitle($faker->title);
            $product->setContent($faker->text);
            $product->setPreContent('dsadasdsaadsadssadasasdsaasd');
            $product->setAuthor($faker->name);
            $product->setCreatedAt($faker->dateTime);
            $manager->persist($product);
        }

        $manager->flush();

    }
}
